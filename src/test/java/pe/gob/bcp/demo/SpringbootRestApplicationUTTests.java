package pe.gob.bcp.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pe.gob.bcp.demo.model.Suma;
import pe.gob.bcp.demo.respository.SumaRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
public class SpringbootRestApplicationUTTests {

    @Autowired
    private SumaRepository sumaRepo;

    @Test
    public void getInsertaSuma() throws Exception {
        Suma suma = new Suma();
        int res = (int) ((Math.random() * (100 - 0)) + 10);
        suma.setId(res);
        suma.setResultado(3);
        sumaRepo.save(suma);
    }
}
