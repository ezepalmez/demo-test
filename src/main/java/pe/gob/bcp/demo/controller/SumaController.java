package pe.gob.bcp.demo.controller;

import java.util.Collections;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.gob.bcp.demo.model.Suma;
import pe.gob.bcp.demo.respository.SumaRepository;

/**
 *
 * @author epalacios
 */
@RestController
@RequestMapping
public class SumaController {

    @Autowired
    private SumaRepository sumaRepo;

    @GetMapping(value = "/suma/{sum01}/{sum02}")
    public @ResponseBody Map sumatoria(@PathVariable("sum01") Integer sum01,@PathVariable("sum02") Integer sum02) {
        Suma suma = new Suma();
        suma.setResultado(sum01+sum02);
        
        sumaRepo.save(suma);
        return Collections.singletonMap("resultado", String.valueOf(sum01+sum02));
    }
}
