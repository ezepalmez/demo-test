/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.bcp.demo.respository;

import org.springframework.data.repository.CrudRepository;
import pe.gob.bcp.demo.model.Suma;

/**
 *
 * @author epalaciosm
 */
public interface SumaRepository extends CrudRepository<Suma, Integer> {   
}
